# Packaging Helmfile

Packaging boilerplate for the buildpacks helmfile cli
(https://github.com/roboll/helmfile)

This is mainly used to deploy helm charts for toolforge

## Building the package

You can just use the build script, pass to it the version of helmfile that you
want, and the version of the debian package build, for example:

```
utils/build_deb.sh 0.145.3 1
```

That will build the package `build/helmfile-0.145.3-1.deb`.

## Uploading the package

Once built, you can upload the debian package with
https://wikitech.wikimedia.org/wiki/Portal:Toolforge/Admin/Packaging#Uploading_a_package
