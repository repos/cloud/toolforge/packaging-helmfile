#!/bin/bash
set -o pipefail
set -o nounset
set -o errexit

HELMFILE_VERSION=${1?No helmfile version passed}
DEB_VERSION=${2:-1}


docker build utils/debuilder-bullseye -t debuilder-bullseye:latest
docker run --volume $PWD:/src:rw --rm debuilder-bullseye:latest "$HELMFILE_VERSION" "$DEB_VERSION"
