#!/bin/bash

set -o pipefail
set -o nounset
set -o errexit

HELMFILE_VERSION=${1?No helmfile version passed}
DEB_VERSION=${2?No deb version passed}
FULL_VERSION=${HELMFILE_VERSION}-${DEB_VERSION}


restore_user() {
    current_user=$(stat . --format=%u)
    current_group=$(stat . --format=%g)
    find . -user root -exec chown $current_user:$current_group {} \;
}

trap restore_user EXIT

cd /src
mkdir -p debian/helmfile/usr/local/bin


URL="https://github.com/roboll/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_linux_amd64"

STATUS_CODE=$(
    curl \
        --silent \
        --location "$URL" \
        -o debian/helmfile/usr/local/bin/helmfile \
        --write-out "%{http_code}"
)

if [[ "$STATUS_CODE" != "200" ]]; then
    echo "Something failed tryng to get version $HELMFILE_VERSION from $URL"
    echo "Got status code $STATUS_CODE"
    exit 1
fi

chmod +x debian/helmfile/usr/local/bin/helmfile

cd debian
sed -e "s/@@FULL_VERSION@@/${FULL_VERSION}/" helmfile/DEBIAN/control.tmpl > helmfile/DEBIAN/control
dpkg-deb --build helmfile "helmfile-${FULL_VERSION}.deb"

cd /src
rm -rf build
mkdir build
mv debian/*.deb build/
echo -e "\n\n###############################\nYour packages can be found now under:"
ls ./build/*
